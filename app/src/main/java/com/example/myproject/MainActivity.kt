package com.example.myproject

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.myproject.app.base.helper.UiHelper
import com.example.myproject.app.navigation.AppNavigation
import com.example.myproject.app.theme.*
import com.google.accompanist.systemuicontroller.rememberSystemUiController

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyProjectTheme() {
                val configuration = LocalConfiguration.current
                val screenWidth = configuration.screenWidthDp
                val screenHeight = configuration.screenHeightDp
                UiHelper.widthScreen = screenWidth
                UiHelper.heightScreen = screenHeight
                HideSystemBars()
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(whiteColor)
                ) {
                    AppNavigation()
                }
            }
        }
    }

    @Composable
    private fun HideSystemBars() {
        val systemUiController = rememberSystemUiController()
        systemUiController.isSystemBarsVisible = false
//        val decorView: View = window.decorView
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
//            val windowInsetsController: WindowInsetsController = decorView.windowInsetsController
//                ?: return
//            // Configure the behavior of the hidden system bars
//            windowInsetsController.systemBarsBehavior =
//                WindowInsetsController.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
//            // Hide both the status bar and the navigation bar
//            windowInsetsController.hide(WindowInsets.Type.systemBars())
//        }
    }
}


@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MyProjectTheme {
        Greeting("Android")
    }
}