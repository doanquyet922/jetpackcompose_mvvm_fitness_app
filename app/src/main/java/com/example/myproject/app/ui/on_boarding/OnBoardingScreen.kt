package com.example.myproject.app.ui.on_boarding

import android.util.Log
import androidx.compose.animation.core.*
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.myproject.R
import com.example.myproject.app.ui.on_boarding.composable.ButtonForward
import com.example.myproject.app.ui.on_boarding.composable.PhaseView

enum class PhaseState(var xPos: Dp) {
    Left(0.dp),
    Mid(0.dp),
    Right(0.dp),
}

@Composable
fun OnBoardingScreen(navigationCallback: () -> Unit) {
    val configuration = LocalConfiguration.current
    val screenWidth = configuration.screenWidthDp.dp
    PhaseState.Left.xPos = -screenWidth
    PhaseState.Right.xPos = screenWidth
    val states = List(listPhase.size) { index ->
        remember {
            if (index == 0) mutableStateOf(PhaseState.Mid) else mutableStateOf(PhaseState.Right)
        }
    }
    var indexPhaseState by remember {
        mutableStateOf(0)
    }

    fun actionContinue() {
        if (indexPhaseState == listPhase.size - 1) {
            navigationCallback()
        } else {
            indexPhaseState++
            states[indexPhaseState - 1].value = PhaseState.Left
            states[indexPhaseState].value = PhaseState.Mid
        }
    }
    for (i: Int in listPhase.indices) {
        val transition = updateTransition(targetState = states[i].value, label = "")
        val xPosDp by transition.animateDp(
            targetValueByState = { it.xPos },
            label = "",
            transitionSpec = {
                tween(
                    durationMillis = 300,
                    easing = LinearEasing
                )
            })
        Box(
            modifier = Modifier.offset(x = xPosDp)
        ) {
            PhaseView(
                imageRes = listPhase[i].image,
                header = listPhase[i].header,
                content = listPhase[i].content
            )
        }
    }
    ButtonForward(
        totalPage = listPhase.size,
        actionContinue = { actionContinue() }
    )
}

class PhaseObject constructor(val image: Int, val header: String, val content: String)

val listPhase = listOf(
    PhaseObject(
        image = R.drawable.phase1,
        header = "Track Your Goal",
        content = "Don't worry if you have trouble determining your goals, We can help you determine your goals and track your goals"
    ),
    PhaseObject(
        image = R.drawable.phase2,
        header = "Get Burn",
        content = "Let’s keep burning, to achive yours goals, it hurts only temporarily, if you give up now you will be in pain forever"
    ),
    PhaseObject(
        image = R.drawable.phase3,
        header = "Eat Well",
        content = "Let's start a healthy lifestyle with us, we can determine your diet every day. healthy eating is fun"
    ),
    PhaseObject(
        image = R.drawable.phase4,
        header = "Improve Sleep  Quality",
        content = "Improve the quality of your sleep with us, good quality sleep can bring a good mood in the morning"
    ),
)
