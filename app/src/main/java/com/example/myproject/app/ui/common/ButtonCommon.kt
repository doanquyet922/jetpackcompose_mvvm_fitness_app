package com.example.myproject.app.ui.common

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.myproject.app.theme.brandColor
import com.example.myproject.app.theme.whiteColor

@Composable
fun ButtonCommon(
    text: String = "",
    gradientButton: Brush? = brandColor,
    gradientText: Brush? = null,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    content: @Composable (RowScope.() -> Unit)? = null
) {
    Row(
        modifier = modifier
            .height(60.dp)
            .fillMaxWidth()
            .clip(MaterialTheme.shapes.large)
            .background(
                brush = gradientButton ?: Brush.horizontalGradient(
                    listOf(
                        whiteColor,
                        whiteColor
                    )
                ),
            )
            .clickable { onClick() },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center,
    ) {
        Text(
            modifier = if (gradientText != null) Modifier
                .graphicsLayer(alpha = 0.99f)
                .drawWithCache {
                    onDrawWithContent {
                        drawContent()
                        drawRect(brandColor, blendMode = BlendMode.SrcAtop)
                    }
                } else Modifier.background(Color.Transparent),
            text = text,
            style = MaterialTheme.typography.subtitle2.copy(
                color = whiteColor,
                fontWeight = FontWeight.W700,
                textAlign = TextAlign.Center,
            )
        )
        if (content != null) {
            Row(content = content)
        }
    }
}