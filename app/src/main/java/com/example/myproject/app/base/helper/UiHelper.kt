package com.example.myproject.app.base.helper

import android.content.res.Resources
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

//SizeScreen
//val configuration = LocalConfiguration.current
//val screenHeight = configuration.screenHeightDp.dp
//val screenWidth = configuration.screenWidthDp.dp
class UiHelper {
    companion object {
        var widthScreen = 0
        var heightScreen = 0
        fun heightResponsive(value: Dp): Dp {
            val heightDesign: Double = 812.0
            return ((heightScreen / heightDesign) * value.value).dp
        }

        fun widthResponsive(value: Dp): Dp {
            val widthDesign: Double = 375.0
            return ((widthScreen / widthDesign) * value.value).dp
        }
    }
}
//final height =
//MediaQueryData.fromWindow(WidgetsBinding.instance.window).size.height;
//final width =
//MediaQueryData.fromWindow(WidgetsBinding.instance.window).size.width;
//return (pixel) * (height / width > 0.46182266 ? (width / 375) : (height / 375));