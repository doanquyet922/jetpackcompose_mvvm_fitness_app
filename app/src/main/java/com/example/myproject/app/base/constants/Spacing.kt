package com.example.myproject.app.base.constants

import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

data class Spacing(
    val superSmall:Dp=5.dp,
    val superExtraSmall:Dp=10.dp,
    val default:Dp=15.dp,
    val extraSmall:Dp=20.dp,
    val small:Dp=25.dp,
    val medium:Dp=30.dp,
    val large:Dp=35.dp,
    val extraLarge:Dp=40.dp,
    val superExtraLarge:Dp=64.dp,
)
val LocalSpacing= compositionLocalOf { Spacing() }