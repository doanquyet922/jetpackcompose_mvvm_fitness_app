package com.example.myproject.app.ui.on_boarding.composable

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.blackColor

@Composable
fun PhaseView(imageRes: Int, header: String, content: String) {
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp.dp
    val screenWidth = configuration.screenWidthDp.dp
    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.Start
    ) {
        Image(
            painter = painterResource(imageRes),
            contentDescription = "",
            modifier = Modifier
                .fillMaxWidth(),
            contentScale = ContentScale.FillWidth
        )
        Text(
            modifier = Modifier.padding(
                top = LocalSpacing.current.superExtraLarge,
                start = LocalSpacing.current.medium,
                end = LocalSpacing.current.medium
            ),
            text = header, style = MaterialTheme.typography.h2.copy(color = blackColor)
        )
        Text(
            modifier = Modifier
                .padding(
                    top = LocalSpacing.current.default,
                    start = LocalSpacing.current.medium,
                    end = LocalSpacing.current.medium
                ),
            text = content,
            style = MaterialTheme.typography.body1
        )
    }
}
