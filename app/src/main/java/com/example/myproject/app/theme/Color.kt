package com.example.myproject.app.theme

import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

//custom Color
val blueColor = Color(0xFF92A3FD)
val blueColor30 = Color(0xFF92A3FD).copy(0.3f)
val lightBlueColor = Color(0xFF9DCEFF)
val purpleColor = Color(0xFFC58BF2)
val purpleColor30 = Color(0xFFC58BF2).copy(0.3f)
val blackColor = Color(0xFF1D1617)
val blackColor30 = Color(0xFF1D1617).copy(0.3f)
val whiteColor = Color(0xFFFFFFFF)
val darkGrayColor = Color(0xff7B6F72)
val grayColor = Color(0xffADA4A5)
val lightGrayColor = Color(0xffDDDADA)
val lightGrayColor30 = Color(0xffDDDADA).copy(0.3f)
val borderColor = Color(0xffF7F8F8)
val dangerColor = Color(0xffFF0000)
val blueShadowColor = Color(0x4D95ADFE).copy(0.3f)
val purpleShadowColor = Color(0x4DC58BF2)

//gradient
val brandColor =
    Brush.horizontalGradient(listOf(Color(0xFF9DCEFF), Color(0xFF92A3FD)))
val brandColor30 =
    Brush.horizontalGradient(listOf(Color(0xFF9DCEFF).copy(0.3f), Color(0xFF92A3FD).copy(0.3f)))
val secondaryColor =
    Brush.horizontalGradient(listOf(Color(0xFFEEA4CE), Color(0xFFC58BF2)))
val gradientVerticalColor =
    Brush.verticalGradient(listOf(Color(0xFF93A7FE), Color(0xFFFFFFFF)))