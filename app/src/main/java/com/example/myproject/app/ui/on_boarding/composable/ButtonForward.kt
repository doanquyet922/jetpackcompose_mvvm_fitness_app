package com.example.myproject.app.ui.on_boarding.composable

import android.annotation.SuppressLint
import androidx.compose.animation.core.*
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.myproject.app.theme.brandColor
import com.example.myproject.R
import com.example.myproject.app.base.constants.LocalSpacing

@SuppressLint("UnusedTransitionTargetStateParameter")
@Composable
fun ButtonForward(totalPage: Int, actionContinue: () -> Unit) {
    val configuration = LocalConfiguration.current
    val screenHeight = configuration.screenHeightDp.dp
    val screenWidth = configuration.screenWidthDp.dp
    val buttonWidth = 60.dp
    var curValue by remember {
        mutableStateOf(0.25f)
    }
    var preValue by remember {
        mutableStateOf(0.25f)
    }
    var animatable = remember {
        Animatable(0f)
    }
    var value by remember {
        mutableStateOf(0f)
    }
    val transition = updateTransition(targetState = value, label = "")
    val degrees by transition.animateFloat(
        targetValueByState = { 360f * (preValue + (curValue - preValue)) },
        label = "",
        transitionSpec = {
            tween(
                durationMillis = 300,
                easing = LinearEasing
            )
        })

    fun actionClick() {
        actionContinue()
        if (curValue == 1f) return
        preValue = curValue
        curValue += (1.0 / totalPage).toFloat()

    }
    Box(
        modifier = Modifier
            .offset(
                x = screenWidth - (buttonWidth + LocalSpacing.current.medium + (animatable.value.dp * 100)),
                y = screenHeight - (buttonWidth + LocalSpacing.current.medium)
            )
            .clickable {
                actionClick()
            },
        contentAlignment = Alignment.Center
    ) {
        Canvas(
            modifier = Modifier
                .size(buttonWidth, buttonWidth)
        ) {
            val canvasWidth = size.width
            val canvasHeight = size.height
            drawCircle(
                brush = brandColor,
                radius = (canvasWidth / 2) - 17f,
            )
            val startAngle = -90f
            val useCenter = false
            drawArc(
                size = Size(width = canvasWidth, height = canvasHeight),
                style = Stroke(width = 9f, cap = StrokeCap.Round),
                brush = brandColor,
                startAngle = startAngle,
                sweepAngle = degrees,
                useCenter = useCenter,
            )
        }
        Image(
            painter = painterResource(R.drawable.ic_arrow_right),
            contentDescription = "",
            modifier = Modifier
                .width(18.dp)
                .height(18.dp)
        )
    }

}
