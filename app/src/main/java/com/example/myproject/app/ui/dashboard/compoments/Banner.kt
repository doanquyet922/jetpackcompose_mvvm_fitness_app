package com.example.myproject.app.ui.dashboard.compoments

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.myproject.R
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.blueShadowColor
import com.example.myproject.app.theme.secondaryColor
import com.example.myproject.app.theme.whiteColor
import com.example.myproject.app.ui.common.ButtonCommon
import com.example.myproject.app.ui.common.coloredShadow
import com.example.myproject.app.base.helper.UiHelper.Companion.widthResponsive as W
import com.example.myproject.app.base.helper.UiHelper.Companion.heightResponsive as H

@Composable
fun Banner() {
    fun onPressViewMore() {}
    Box(
        modifier = Modifier
            .padding(top = H(LocalSpacing.current.medium))
            .coloredShadow(
                color = blueShadowColor,
                blurRadius = 22.dp,
                offsetY = 10.dp
            )
    ) {
        Image(
            painter = painterResource(id = R.drawable.img_banner),
            contentDescription = "",
            contentScale = ContentScale.FillWidth,
            modifier = Modifier.fillMaxWidth()
        )
        Row {
            Column(
                modifier = Modifier.padding(
                    top = H(LocalSpacing.current.small),
                    start = W(LocalSpacing.current.extraSmall)
                )
            ) {
                Text(
                    text = "BMI (Body Mass Index)",
                    style = MaterialTheme.typography.body1.copy(
                        color = whiteColor,
                        fontWeight = FontWeight.W600
                    )
                )
                Text(
                    modifier = Modifier
                        .padding(top = H(LocalSpacing.current.superSmall)),
                    text = "You have a normal weight",
                    style = MaterialTheme.typography.body2.copy(
                        color = whiteColor,
                    )
                )
                ButtonCommon(
                    onClick = { onPressViewMore() },
                    gradientButton = secondaryColor,
                    modifier = Modifier
                        .padding(top = H(LocalSpacing.current.default))
                        .width(W(95.dp))
                        .height(H(40.dp))
                ) {
                    Text(
                        text = "View More", style = MaterialTheme.typography.caption.copy(
                            color = whiteColor,
                            fontWeight = FontWeight.W600
                        )
                    )
                }
            }
            Spacer(modifier = Modifier.weight(1f))
            Box(
                contentAlignment = Alignment.TopEnd,
                modifier = Modifier.padding(
                    top = H(LocalSpacing.current.small),
                    end = W(LocalSpacing.current.extraSmall)
                )
            )
            {
                Image(
                    painter = painterResource(id = R.drawable.img_banner_pie),
                    contentDescription = "",
                    modifier = Modifier
                        .height(H(106.dp))
                        .width(W(106.dp))
                )
                Text(
                    modifier = Modifier
                        .padding(
                            top = H(LocalSpacing.current.extraSmall),
                        )
                        .width(W(70.dp)),
                    textAlign = TextAlign.Center,
                    text = "30,3", style = MaterialTheme.typography.body2.copy(
                        color = whiteColor,
                        fontWeight = FontWeight.W600
                    )
                )
            }

        }
    }
}