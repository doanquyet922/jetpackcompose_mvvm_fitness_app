package com.example.myproject.app.ui.intro_register

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.myproject.R
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.base.helper.UiHelper.Companion.widthResponsive
import com.example.myproject.app.base.helper.UiHelper.Companion.heightResponsive
import com.example.myproject.app.theme.*
import com.example.myproject.app.ui.common.ButtonCommon
import com.example.myproject.app.ui.common.coloredShadow
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState

@OptIn(ExperimentalPagerApi::class)
@Composable
fun IntroRegisterScreen() {
    fun onPressConfirm() {

    }

    val pagerState =
        rememberPagerState(pageCount = listIntroRegister.size, initialPage = 0, infiniteLoop = true)
    Column(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            modifier = Modifier.padding(
                top = heightResponsive(LocalSpacing.current.extraLarge),
                bottom = heightResponsive(LocalSpacing.current.superSmall)
            ),
            text = "What is your goal ?",
            style = MaterialTheme.typography.h4,
        )
        Text(
            modifier = Modifier
                .padding(
                    bottom = heightResponsive(50.dp)
                )
                .width(widthResponsive(182.dp)),
            text = "It will help us to choose a best program for you",
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.body2.copy(color = darkGrayColor),
        )
        HorizontalPager(
            modifier = Modifier.fillMaxWidth(),
            itemSpacing = 30.dp,
            state = pagerState
        ) { page ->
            Column(
                modifier = Modifier
                    .padding(
                        top =
                        if (pagerState.currentPage == page) 0.dp
                        else heightResponsive(40.dp)
                    )
                    .width(widthResponsive(275.dp))
                    .height(
                        if (pagerState.currentPage == page) heightResponsive(478.dp)
                        else heightResponsive(358.dp)
                    )
                    .coloredShadow(
                        blurRadius = 22.dp,
                        color = if (pagerState.currentPage == page) purpleColor30 else Color.Transparent,
                        offsetY = 10.dp,
                    )
                    .clip(RoundedCornerShape(22.dp))
                    .background(brush = if (pagerState.currentPage == page) brandColor else brandColor30),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Box(modifier = Modifier.weight(1f)) {
                    Image(
                        painter = painterResource(id = listIntroRegister[page].image),
                        contentDescription = "",
                        modifier = Modifier
                            .padding(
                                widthResponsive(LocalSpacing.current.extraSmall)
                            )
                            .fillMaxSize()
                    )
                }
                Text(
                    text = listIntroRegister[page].header,
                    style = MaterialTheme.typography.body1.copy(color = whiteColor)
                )
                Box(
                    modifier = Modifier
                        .padding(
                            top = heightResponsive(3.dp),
                            bottom = heightResponsive(
                                LocalSpacing.current.extraSmall
                            )
                        )
                        .width(widthResponsive(50.dp))
                        .height(heightResponsive(1.dp))
                        .background(color = whiteColor)

                )
                Text(
                    modifier = Modifier
                        .padding(
                            start = widthResponsive(LocalSpacing.current.medium),
                            bottom = heightResponsive(LocalSpacing.current.medium),
                            end = widthResponsive(LocalSpacing.current.medium)
                        ),
                    text = listIntroRegister[page].content,
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.body2.copy(color = whiteColor)
                )
            }

        }
        Box(
            modifier = Modifier
                .padding(
                    start = widthResponsive(LocalSpacing.current.medium),
                    end = widthResponsive(LocalSpacing.current.medium),
                    bottom = heightResponsive(LocalSpacing.current.extraLarge)
                )
                .weight(1f),
            contentAlignment = Alignment.BottomCenter

        ) {
            ButtonCommon(text = "Confirm", onClick = { onPressConfirm() })
        }
    }
}

class IntroRegisterObject constructor(val image: Int, val header: String, val content: String)

val listIntroRegister = listOf(
    IntroRegisterObject(
        image = R.drawable.img_intro1,
        header = "Improve Shape",
        content = "I’m “skinny fat”. look thin but have no shape. I want to add learn muscle in the right way"
    ),
    IntroRegisterObject(
        image = R.drawable.img_intro2,
        header = "Lean & Tone",
        content = "Let’s keep burning, to achive yours goals, it hurts only temporarily, if you give up now you will be in pain forever"
    ),
    IntroRegisterObject(
        image = R.drawable.img_intro3,
        header = "Lose a Fat",
        content = "I’m “skinny fat”. look thin but have no shape. I want to add learn muscle in the right way"
    ),
)
