package com.example.myproject.app.ui.dashboard.compoments.activity_status

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.myproject.app.ui.dashboard.compoments.activity_status.conpoments.HeartRateGraph
import java.lang.Long.MAX_VALUE
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

@Composable
fun ActivityStatus() {

    Column() {
        Text(
            text = "Activity Status",
            style = MaterialTheme.typography.subtitle2.copy(fontWeight = FontWeight.W600)
        )
        HeartRateGraph()
    }
}

