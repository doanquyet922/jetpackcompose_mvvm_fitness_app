package com.example.myproject.app.ui.dashboard

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.ui.dashboard.compoments.Action
import com.example.myproject.app.ui.dashboard.compoments.activity_status.ActivityStatus
import com.example.myproject.app.ui.dashboard.compoments.Banner
import com.example.myproject.app.ui.dashboard.compoments.HeaderDashboard
import com.example.myproject.app.base.helper.UiHelper.Companion.widthResponsive as W

@Composable
fun DashboardScreen() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = W(LocalSpacing.current.medium))
    ) {
        HeaderDashboard()
        Banner()
        Action()
        ActivityStatus()
    }
}



