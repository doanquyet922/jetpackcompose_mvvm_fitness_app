package com.example.myproject.app.ui.dashboard.compoments

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.brandColor
import com.example.myproject.app.theme.whiteColor
import com.example.myproject.app.ui.common.ButtonCommon
import com.example.myproject.app.base.helper.UiHelper.Companion.widthResponsive as W
import com.example.myproject.app.base.helper.UiHelper.Companion.heightResponsive as H

@Composable
fun Action() {
    fun onPressCheck() {

    }
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .padding(vertical = H(LocalSpacing.current.medium))
            .fillMaxWidth()
            .height(H(57.dp))
            .background(brush = brandColor, shape = RoundedCornerShape(16.dp), alpha = 0.2f)
            .padding(horizontal = H(LocalSpacing.current.extraSmall)),
    ) {
        Text(
            text = "Today Target",
            style = MaterialTheme.typography.body1.copy(fontWeight = FontWeight.W500)
        )
        ButtonCommon(
            onClick = { onPressCheck() },
            gradientButton = brandColor,
            modifier = Modifier
                .width(W(68.dp))
                .height(H(28.dp))
        ) {
            Text(
                text = "Check",
                style = MaterialTheme.typography.body2.copy(color = whiteColor)
            )
        }
    }
}