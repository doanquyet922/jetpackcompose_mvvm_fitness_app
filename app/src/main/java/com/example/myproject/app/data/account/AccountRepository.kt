package com.example.myproject.app.data.account

import com.example.myproject.app.data.account.model.Account
import com.example.myproject.app.data.account.service.AccountService

class AccountRepository(private val accountService: AccountService= AccountService()){
    suspend fun login(email: String, password: String): Result<Account> {
        return accountService.login(email, password)
    }
    companion object {
        @Volatile
        private var instance: AccountRepository? = null

        fun getInstance() = instance?: synchronized(this) {
            instance ?: AccountRepository().also { instance = it }
        }
    }
}