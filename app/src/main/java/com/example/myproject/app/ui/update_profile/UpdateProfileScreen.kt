package com.example.myproject.app.ui.update_profile

import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import com.example.myproject.R
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.darkGrayColor
import com.example.myproject.app.theme.secondaryColor
import com.example.myproject.app.theme.whiteColor
import com.example.myproject.app.ui.common.ButtonCommon
import com.example.myproject.app.ui.common.TextFieldCommon

@Composable
fun UpdateProfileScreen(navigationCallback: () -> Unit) {
    var textGender by remember {
        mutableStateOf("")
    }
    var textDate by remember {
        mutableStateOf("")
    }

    var textWeight by remember {
        mutableStateOf("")
    }
    var textHeight by remember {
        mutableStateOf("")
    }

    fun onClickGender() {
        println("onClickGender")
        textGender = "Man"
    }

    fun onClickDate() {
        println("onClickDate")
        textDate = "2000"
    }

    fun onChangeWeight(it: String) {
        textWeight = it
    }

    fun onChangeHeight(it: String) {
        textHeight = it
    }

    fun toIntroRegister() {
        navigationCallback()
    }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .verticalScroll(rememberScrollState())

    ) {
        Image(
            painter = painterResource(id = R.drawable.img_update_profile),
            contentDescription = "",
            modifier = Modifier.fillMaxWidth(),
            contentScale = ContentScale.FillWidth
        )
        Text(
            modifier = Modifier.padding(
                top = LocalSpacing.current.medium,
                bottom = LocalSpacing.current.superSmall
            ),
            text = "Let’s complete your profile",
            style = MaterialTheme.typography.h4,
        )
        Text(
            text = "It will help us to know more about you!",
            style = MaterialTheme.typography.body2.copy(color = darkGrayColor),
        )
        Column(
            modifier = Modifier.padding(
                LocalSpacing.current.medium
            ),
        ) {
            TextFieldCommon(
                value = textGender,
                enabled = false,
                placeholderString = "Choose Gender",
                leadingIconVector = ImageVector.vectorResource(
                    id = R.drawable.ic_user
                ),
                trailingIconVector = ImageVector.vectorResource(
                    id = R.drawable.ic_dropdown
                ),
                onClickTrailing = {
                    onClickGender()
                }
            )
            Spacer(
                modifier = Modifier.height(
                    LocalSpacing.current.default
                ),
            )
            Box(
                modifier = Modifier
                    .clip(MaterialTheme.shapes.medium)
                    .clickable { onClickDate() }
            ) {
                TextFieldCommon(
                    value = textDate,
                    enabled = false,
                    placeholderString = "Date of Birth",
                    leadingIconVector = ImageVector.vectorResource(
                        id = R.drawable.ic_calendar
                    ),
                )
            }
            Spacer(
                modifier = Modifier.height(
                    LocalSpacing.current.default
                ),
            )
            Row {
                Box(
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = LocalSpacing.current.default)
                ) {
                    TextFieldCommon(
                        value = textWeight,
                        onValueChange = { onChangeWeight(it) },
                        placeholderString = "Your Weight",
                        leadingIconVector = ImageVector.vectorResource(
                            id = R.drawable.ic_weight_scale
                        ),
                    )
                }
                Box(
                    modifier = Modifier
                        .width(48.dp)
                        .height(48.dp)
                        .background(brush = secondaryColor, shape = MaterialTheme.shapes.medium),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "KG",
                        style = MaterialTheme.typography.body2.copy(color = whiteColor),
                    )
                }
            }
            Spacer(
                modifier = Modifier.height(
                    LocalSpacing.current.default
                ),
            )
            Row {
                Box(
                    modifier = Modifier
                        .weight(1f)
                        .padding(end = LocalSpacing.current.default)
                ) {
                    TextFieldCommon(
                        value = textHeight,
                        onValueChange = { onChangeHeight(it) },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        placeholderString = "Your Height",
                        leadingIconVector = ImageVector.vectorResource(
                            id = R.drawable.ic_swap
                        ),
                    )
                }
                Box(
                    modifier = Modifier
                        .width(48.dp)
                        .height(48.dp)
                        .background(brush = secondaryColor, shape = MaterialTheme.shapes.medium),
                    contentAlignment = Alignment.Center
                ) {
                    Text(
                        text = "CM",
                        style = MaterialTheme.typography.body2.copy(color = whiteColor),
                    )
                }
            }
        }
        Box(
            modifier = Modifier.padding(
                start = LocalSpacing.current.extraLarge,
                end = LocalSpacing.current.extraLarge,
                bottom = LocalSpacing.current.extraLarge
            )
        ) {
            ButtonCommon(text = "Next", onClick = { toIntroRegister() }) {
                Image(
                    modifier = Modifier
                        .padding(start = 3.dp, bottom = 3.dp)
                        .width(20.dp)
                        .height(20.dp),
                    imageVector = ImageVector.vectorResource(id = R.drawable.ic_arrow_right),
                    contentDescription = "",
                    alignment = Alignment.Center
                )
            }
        }
    }
}