package com.example.myproject.app.data.account.model

import com.google.gson.annotations.SerializedName

data class Accounts(val accounts: List<Account>)

data class Account(
    @SerializedName("idAccount") val id: String,
    @SerializedName("username") val username: String,
    @SerializedName("email") val email: String,
    @SerializedName("password") val password: String,
    @SerializedName("avatar") val avatar: String,
)