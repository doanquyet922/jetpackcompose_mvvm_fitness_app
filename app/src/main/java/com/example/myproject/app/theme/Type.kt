package com.example.myproject.app.theme

import androidx.compose.material.Typography
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.myproject.R
import com.example.myproject.app.base.helper.UiHelper

val Poppins = FontFamily(
    Font(R.font.poppins_regular, FontWeight.W400),
    Font(R.font.poppins_medium, FontWeight.W500),
    Font(R.font.poppins_semibold, FontWeight.W600),
    Font(R.font.poppins_bold, FontWeight.W700),
)

// Set of Material typography styles to start with
val Typography = Typography(
    h1 = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W700,
        fontSize = (26.0).sp,
        lineHeight = 39.sp,
        color = blackColor
    ),
    h2 = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W700,
        fontSize = (24.0).sp,
        lineHeight = (36).sp,
        color = blackColor
    ),
    h3 = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W700,
        fontSize = (22.0).sp,
        lineHeight = (33).sp,
        color = blackColor
    ),
    h4 = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W700,
        fontSize = (20.0).sp,
        lineHeight = (39).sp,
        color = blackColor
    ),
    subtitle1 = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W400,
        fontSize = (18.0).sp,
        lineHeight = (27).sp,
        color = blackColor
    ),
    subtitle2 = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W400,
        fontSize = (16.0).sp,
        lineHeight = (24).sp,
        color = blackColor
    ),
    body1 = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W400,
        fontSize = (14.0).sp,
        lineHeight = (21).sp,
        color = blackColor
    ),
    body2 = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W400,
        fontSize = (12.0).sp,
        lineHeight = (18).sp,
        color = blackColor
    ),
    caption = TextStyle(
        fontFamily = Poppins,
        fontWeight = FontWeight.W400,
        fontSize = (10.0).sp,
        lineHeight = (15).sp,
        color = blackColor
    ),

/* Other default text styles to override
button = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.W500,
    fontSize = 14.sp
),
caption = TextStyle(
    fontFamily = FontFamily.Default,
    fontWeight = FontWeight.Normal,
    fontSize = 12.sp
)
*/
)