package com.example.myproject.app.navigation
sealed class Screen(open val route: String = "") {
    object GetStarted : Screen("get-started")
    object OnBoarding : Screen("on-boarding")
    object SignUp : Screen("sign-up")
    object UpdateProfile : Screen("update-profile")
    object IntroRegister : Screen("intro-register")
    object Login : Screen("login")
    object Welcome : Screen("welcome")
    object Dashboard : Screen("dashboard")
    object Example : Screen("example/{episodeUri}") {
        fun createRoute(episodeUri: String) = "example/$episodeUri"
    }
}
