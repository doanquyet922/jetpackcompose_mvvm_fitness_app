package com.example.myproject.app.ui.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.blueColor
import com.example.myproject.app.theme.borderColor
import com.example.myproject.app.theme.grayColor
import com.example.myproject.app.theme.lightGrayColor

@Composable
fun TextFieldCommon(
    value: String,
    onValueChange: (String) -> Unit = {},
    placeholderString: String = "",
    leadingIconVector: ImageVector,
    trailingIconVector: ImageVector? = null,
    onClickTrailing: () -> Unit = {},
    enabled: Boolean = true,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    passwordVisible: Boolean = false
) {
    Row(
        modifier = Modifier
            .height(48.dp)
            .background(
                color = borderColor,
                shape = MaterialTheme.shapes.medium
            ),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        BasicTextField(
            keyboardOptions = keyboardOptions,
            cursorBrush = SolidColor(blueColor),
            value = value,
            onValueChange = onValueChange,
            singleLine = true,
            enabled = enabled,
            visualTransformation = if (!passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
            textStyle = MaterialTheme.typography.body2.copy(color = grayColor),
            decorationBox = { innerTextField ->
                Row() {
                    Image(
                        imageVector = leadingIconVector,
                        contentDescription = "",
                        modifier = Modifier
                            .padding(
                                start = LocalSpacing.current.default,
                                end = LocalSpacing.current.superExtraSmall
                            )
                            .height(18.dp)
                            .width(18.dp),
                    )
                    Box(modifier = Modifier.weight(1f)) {
                        if (value.isEmpty()) {
                            Text(
                                text = placeholderString,
                                style = MaterialTheme.typography.body2.copy(color = grayColor)
                            )
                        }
                        innerTextField()
                    }
                    if (trailingIconVector != null) {
                        Image(
                            imageVector = trailingIconVector,
                            contentDescription = "",
                            modifier = Modifier
                                .padding(
                                    start = LocalSpacing.current.superSmall,
                                    end = LocalSpacing.current.default
                                )
                                .height(18.dp)
                                .width(18.dp)
                                .clickable { onClickTrailing() },
                        )
                    }

                }

            }
        )
    }
}