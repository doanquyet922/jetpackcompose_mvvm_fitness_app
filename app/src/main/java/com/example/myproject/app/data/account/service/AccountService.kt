package com.example.myproject.app.data.account.service

import com.example.myproject.app.data.account.model.Account
import kotlinx.coroutines.delay

class AccountService {
    suspend fun login(email: String, password: String): Result<Account> {
        println("AccountService-Login loading:\nemail:${email}\npassword:${password}")
        delay(2000)
        return if (email == "account1@gmail.com" && password == "123456")
            Result.success(
                Account(
                    id = "1",
                    username = "Account 1",
                    email = "account1@gmail.com",
                    password = "123456",
                    avatar = ""
                )
            )
        else Result.failure(exception = Throwable(message = "Email Or password incorrect"))
    }
}
//class MealsWebService {
//
//    private lateinit var api: MealsApi
//
//    init {
//        val retrofit = Retrofit.Builder()
//            .baseUrl("https://www.themealdb.com/api/json/v1/1/")
//            .addConverterFactory(GsonConverterFactory.create())
//            .build()
//
//        api = retrofit.create(MealsApi::class.java)
//    }
//
//    suspend fun getMeals(): MealsCategoriesResponse {
//        return api.getMeals()
//    }
//
//    interface MealsApi {
//        @GET("categories.php")
//        suspend fun getMeals(): MealsCategoriesResponse
//    }
//
//}