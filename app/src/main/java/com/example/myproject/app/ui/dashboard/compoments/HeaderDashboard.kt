package com.example.myproject.app.ui.dashboard.compoments

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.example.myproject.R
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.*
import com.example.myproject.app.base.helper.UiHelper.Companion.widthResponsive as W
import com.example.myproject.app.base.helper.UiHelper.Companion.heightResponsive as H

@Composable
fun HeaderDashboard() {
    var isNotification by remember { mutableStateOf(true) }
    Row(
        modifier = Modifier
            .padding(top = H(LocalSpacing.current.extraLarge))
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Column() {
            Text(
                text = "Welcome Back,",
                style = MaterialTheme.typography.body2.copy(color = grayColor)
            )
            Text(text = "Stefani Wong", style = MaterialTheme.typography.h4)
        }
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .width(W(40.dp))
                .height(H(40.dp))
                .clip(RoundedCornerShape(8.dp))
                .background(borderColor)
                .clickable { },

            ) {
            Image(
                imageVector =
                if (isNotification)
                    ImageVector.vectorResource(R.drawable.ic_notification_enable)
                else ImageVector.vectorResource(R.drawable.ic_notification),
                contentDescription = "",
                modifier = Modifier
                    .width(W(18.dp))
                    .height(H(18.dp))
            )
        }

    }
}