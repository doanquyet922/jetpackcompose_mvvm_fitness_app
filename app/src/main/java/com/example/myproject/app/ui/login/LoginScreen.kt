package com.example.myproject.app.ui.login

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.myproject.R
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.*
import com.example.myproject.app.ui.common.ButtonCommon
import com.example.myproject.app.ui.common.LoadingGlobal
import com.example.myproject.app.ui.common.TextFieldCommon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Composable
fun LoginScreen(
    navigationToRegisterCallback: () -> Unit,
    navigationToWelcomeCallback: () -> Unit
) {

    val viewModel: LoginViewModel = viewModel()
    var textEmail by remember { mutableStateOf("") }
    var textPassword by remember { mutableStateOf("") }
    var passwordVisible by remember { mutableStateOf(true) }
    fun onChangePasswordVisible() {
        passwordVisible = !passwordVisible
    }

    fun onPressForgotPassword() {
        Log.d("BBB", "onPressForgotPassword")
    }

    fun onPressEmail() {
        Log.d("BBB", "onPressEmail")
    }

    fun onPressFacebook() {
        Log.d("BBB", "onPressFacebook")
    }

    fun onPressLogin(context: Context) {
        Log.d("BBB", "toLogin")
        viewModel.login(textEmail, textPassword,
            onLoginSuccess = {
                println("onLoginSuccess")
                viewModel.viewModelScope.launch(Dispatchers.Main) {
                    navigationToWelcomeCallback()
                }
            },
            onLoginFailure = { error: String? ->
                viewModel.viewModelScope.launch(Dispatchers.Main) {
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                }
            })
    }

    fun onPressRegister() {
        Log.d("BBB", "onPressRegister")
        navigationToRegisterCallback()
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(
                horizontal = LocalSpacing.current.medium,
                vertical = LocalSpacing.current.extraLarge
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        Text(
            text = "Hey there,",
            style = MaterialTheme.typography.subtitle2,
        )
        Text(
            modifier = Modifier.padding(
                bottom = LocalSpacing.current.medium
            ),
            text = "Welcome Back",
            style = MaterialTheme.typography.h4,
        )
        TextFieldCommon(
            value = textEmail,
            onValueChange = { textEmail = it },
            placeholderString = "Email",
            leadingIconVector = ImageVector.vectorResource(R.drawable.ic_message)
        )
        Spacer(modifier = Modifier.height(LocalSpacing.current.default))
        TextFieldCommon(
            value = textPassword,
            onValueChange = { textPassword = it },
            placeholderString = "Password",
            leadingIconVector = ImageVector.vectorResource(R.drawable.ic_lock),
            trailingIconVector = ImageVector.vectorResource(R.drawable.ic_hide_password),
            onClickTrailing = { onChangePasswordVisible() },
            passwordVisible = passwordVisible
        )
        Spacer(modifier = Modifier.height(LocalSpacing.current.superExtraSmall))
        Text(
            modifier = Modifier.clickable { onPressForgotPassword() },
            text = "Forgot your password?",
            style = MaterialTheme.typography.body2.copy(
                color = grayColor,
                fontWeight = FontWeight.W500,
                textAlign = TextAlign.Center,
                textDecoration = TextDecoration.Underline
            )
        )
        Spacer(
            modifier = Modifier
                .weight(1f)
        )
        val mContext = LocalContext.current
        ButtonCommon(onClick = { onPressLogin(mContext) }) {
            Image(
                imageVector = ImageVector.vectorResource(id = R.drawable.ic_login),
                contentDescription = "",
                modifier = Modifier
                    .padding(end = LocalSpacing.current.superSmall)
                    .width(24.dp)
                    .height(24.dp)
            )
            Text(
                text = "Login",
                style = MaterialTheme.typography.subtitle2.copy(
                    color = whiteColor,
                    fontWeight = FontWeight.W700,
                    textAlign = TextAlign.Center,
                )
            )
        }
        Row(
            modifier = Modifier.padding(vertical = LocalSpacing.current.extraSmall),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(
                modifier = Modifier
                    .height(1.dp)
                    .weight(1f)
                    .background(color = lightGrayColor)
            )
            Text(
                modifier = Modifier.padding(horizontal = LocalSpacing.current.superExtraSmall),
                text = "Or",
                style = MaterialTheme.typography.body2
            )
            Spacer(
                modifier = Modifier
                    .height(1.dp)
                    .weight(1f)
                    .background(color = lightGrayColor)
            )
        }
        Row {
            Image(
                modifier = Modifier
                    .height(50.dp)
                    .width(50.dp)
                    .clickable { onPressEmail() },
                painter = painterResource(id = R.drawable.ic_gmail),
                contentDescription = ""
            )
            Spacer(modifier = Modifier.width(LocalSpacing.current.medium))
            Image(
                modifier = Modifier
                    .height(50.dp)
                    .width(50.dp)
                    .clickable { onPressFacebook() },
                painter = painterResource(id = R.drawable.ic_facebook),
                contentDescription = ""
            )
        }
        Row(
            modifier = Modifier.padding(
                top = LocalSpacing.current.medium,
            ),
        ) {
            Text(
                text = "Don’t have an account yet? ",
                style = MaterialTheme.typography.body1
            )
            Text(
                modifier = Modifier.clickable { onPressRegister() },
                text = "Register",
                style = MaterialTheme.typography.body1.copy(color = purpleColor)
            )
        }
    }
    LoadingGlobal(isShowDialog = viewModel.isLoading.value)
}

