package com.example.myproject.app.ui.welcome

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.base.helper.UiHelper.Companion.widthResponsive as W
import com.example.myproject.app.base.helper.UiHelper.Companion.heightResponsive as H
import com.example.myproject.app.theme.darkGrayColor
import com.example.myproject.app.ui.common.ButtonCommon
import com.example.myproject.R

@Composable
fun WelcomeScreen(navigationCallback: () -> Unit) {
    fun goToHome() {
        navigationCallback()
    }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        Column(
            modifier = Modifier.weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Image(
                painter = painterResource(id = R.drawable.img_welcome), contentDescription = "",
                modifier = Modifier
                    .width(W(277.56.dp))
                    .height(H(304.dp))
            )
            Text(
                modifier = Modifier.padding(
                    top = H(LocalSpacing.current.extraLarge),
                    bottom = H(LocalSpacing.current.superSmall)
                ),
                text = "Welcome, Stefani",
                style = MaterialTheme.typography.h4,
            )
            Text(
                modifier = Modifier
                    .width(W(214.dp)),
                text = "You are all set now, let’s reach your goals together with us",
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.body2.copy(color = darkGrayColor),
            )

        }
        Box(
            modifier = Modifier.padding(
                horizontal = W(LocalSpacing.current.medium),
                vertical = H(LocalSpacing.current.extraLarge)
            )
        ) {
            ButtonCommon(text = "Go To Home", onClick = { goToHome() })
        }
    }

}