package com.example.myproject.app.ui.common

import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.*
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.SecureFlagPolicy

@Composable
fun LoadingGlobal(isShowDialog: Boolean) {
    if (isShowDialog)
        Dialog(
            onDismissRequest = { println("onDismissRequest") },
            properties = DialogProperties(
                securePolicy = SecureFlagPolicy.SecureOn
            )
        ) {
            CircularProgressIndicator()
        }
}