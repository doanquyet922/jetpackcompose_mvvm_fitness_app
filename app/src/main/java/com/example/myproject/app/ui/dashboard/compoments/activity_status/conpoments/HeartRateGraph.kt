package com.example.myproject.app.ui.dashboard.compoments.activity_status.conpoments

import android.graphics.Paint
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.semantics.SemanticsProperties.Text
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt
import com.example.myproject.app.base.helper.UiHelper.Companion.widthResponsive as W
import com.example.myproject.app.base.helper.UiHelper.Companion.heightResponsive as H

@Composable
fun HeartRateGraph() {
    val now = Date()
//    Box(
//        modifier = Modifier
//            .fillMaxWidth()
//            .height(H(150.dp))
//            .background(brush = brandColor, shape = RoundedCornerShape(20), alpha = 0.2f)
//    ) {
    SuperSimpleLineChart(
        heartRates = heartRates,
        currentTime = now, modifier = Modifier
            .fillMaxWidth()
            .height((150.dp))
    )

//    }
}


//point representation
data class HeartRate(val timeString: String, val bpm: Int)
data class Point(val x: Float, val y: Float)

fun pxToFloat(pixel: Double): Float {
    return pixel.toFloat() * (395f / 150f)
}

@Composable
fun SuperSimpleLineChart(
    currentTime: Date,
    heartRates: List<HeartRate>,
    modifier: Modifier = Modifier.size(300.dp, 55.dp)
) {

    // our values to draw
    var listRangeTimeToFloat = heartRates.map { heartRate ->
        val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return@map getLegacyDateDifference(
            fromDate = heartRate.timeString,
            toDate = formatter.format(currentTime)
        ).getValue("seconds").toFloat()
    }
    listRangeTimeToFloat = listRangeTimeToFloat.reversed()
    var values: ArrayList<Point> = ArrayList()
    for (i in heartRates.indices) {
        values.add(Point(x = listRangeTimeToFloat[i], y = heartRates[i].bpm.toFloat()))
    }
    // find max and min value of X, we will need that later
    val minXValue = values.minOf { it.x }
    val maxXValue = values.maxOf { it.x }

    // find max and min value of Y, we will need that later
    val minYValue = values.minOf { it.y }
    val maxYValue = values.maxOf { it.y }
    var offset by remember { mutableStateOf(0f) }
    var point by remember { mutableStateOf(Point(0f, 0f)) }
    var heartRate by remember { mutableStateOf(heartRates[0]) }
    var widthBox by remember { mutableStateOf(0) }
    var isScroll by remember { mutableStateOf(false) }
    var rangeTimeToNow by remember { mutableStateOf(0f) }
//    Log.d("BBB", "widthBox:" + widthBox)
    // create Box with canvas
    Box(modifier = modifier
        .background(brush = brandColor, shape = RoundedCornerShape(20), alpha = 0.2f)
        .onGloballyPositioned { coordinates ->
            // This will be the size of the Column.
            if (widthBox != coordinates.size.width) {
                widthBox = coordinates.size.width
            }
            // The position of the Column relative to the application window.
//            coordinates.positionInWindow()
//            // The position of the Column relative to the Compose root.
//            coordinates.positionInRoot()
//            // These will be the alignment lines provided to the layout (empty here for Column).
//            coordinates.providedAlignmentLines
//            // This will be a LayoutCoordinates instance corresponding to the parent of Column.
//            coordinates.parentLayoutCoordinates
        }
        .scrollable(
            orientation = Orientation.Horizontal,
            // Scrollable state: describes how to consume
            // scrolling delta and update offset
            state = rememberScrollableState { delta ->
                isScroll = true
                offset += delta
                if (offset < 0.0f) {
                    offset = 0.0f
                }
                if (offset >= widthBox.toFloat()) {
                    offset = widthBox.toFloat()
                }
                delta
            }
        )
        .drawBehind {
            val yBegin = size.height - pxToFloat(34.5)
            // we use drawBehind() method to create canvas
            // map data points to pixel values, in canvas we think in pixels
            val pixelPoints = values.map {
                // we use extension function to convert and scale initial values to pixels
                val x = it.x.mapValueToDifferentRange(
                    inMin = minXValue,
                    inMax = maxXValue,
                    outMin = 0f,
                    outMax = size.width
                )

                // same with y axis
                val y = it.y.mapValueToDifferentRange(
                    inMin = minYValue,
                    inMax = maxYValue,
                    outMin = yBegin,
                    outMax = pxToFloat(60.0)
                )
//                val y = it.y.mapValueToDifferentRange(
//                    inMin = minYValue,
//                    inMax = maxYValue,
//                    outMin = size.height - (34.5f * (395f / 150f)),
//                    outMax = (60f * (395f / 150f))
//                )
                Point(x, y)
            }
//            Log.d(
//                "BBB",
//                "outMin:" + pixelPoints[0].x + "-Max:" + pixelPoints[pixelPoints.size - 1].x
//            )
            if (!isScroll) {
                offset = pixelPoints[23].x
                point = pixelPoints[23]
                heartRate = heartRates[23]
            }
            // Find "point" the selected point displayed
            // And set value "offset"
            when (offset) {
                0.0f -> {
                    point = pixelPoints[0]
                    heartRate = heartRates[0]
                }
                pixelPoints[pixelPoints.size - 1].x -> {
                    point = pixelPoints[pixelPoints.size - 1]
                    heartRate = heartRates[heartRates.size - 1]
                }
                else -> for (i in 0 until pixelPoints.size - 2) {
                    val p1 = pixelPoints[i]
                    val p2 = pixelPoints[i + 1]

                    if (p1.x < offset && offset < p2.x) {
                        val t1 = offset - p1.x
                        val t2 = p2.x - offset
                        point = if (t1 > t2) {
                            p2
                        } else p1
                        heartRate = if (t1 > t2) {
                            heartRates[i + 1]
                        } else heartRates[i]
                    }

                }
            }
            var pathGradientDrop = Path()
            pathGradientDrop.moveTo(0f, yBegin + 20f)
            pathGradientDrop.lineTo(pixelPoints[0].x, pixelPoints[0].y)
            var pathLineDrop = Path()
            val path = Path() // prepare p0ath to draw
            // in the loop below we fill our path
            var isEndPointGradient = false
            pixelPoints.forEachIndexed { index, pixelPoint ->
                if (index == 0) { // for the first point we just move drawing cursor to the position
                    path.moveTo(pixelPoint.x, pixelPoint.y)
                    pathLineDrop.moveTo(pixelPoint.x, pixelPoint.y)
                } else {
                    path.lineTo(
                        pixelPoint.x,
                        pixelPoint.y
                    ) // for rest of points we draw the line

                    if (!isEndPointGradient) {
                        pathGradientDrop.lineTo(
                            pixelPoint.x,
                            pixelPoint.y
                        )
                        pathLineDrop.lineTo(
                            pixelPoint.x,
                            pixelPoint.y
                        )
                    }

                    if (point == pixelPoint) {
                        isEndPointGradient = true
                    }
                }

            }
            for (index in pixelPoints.indices) {
                if (index == 0) { // for the first point we just move drawing cursor to the position
                    path.moveTo(pixelPoints[index].x, pixelPoints[index].y)
                } else {
                    path.lineTo(
                        pixelPoints[index].x,
                        pixelPoints[index].y
                    ) // for rest of points we draw the line
                    if (point == pixelPoints[index])
                        pathGradientDrop.lineTo(
                            pixelPoints[index].x,
                            pixelPoints[index].y
                        )
                }
            }
            pathGradientDrop.lineTo(point.x, yBegin + 20f)
            pathGradientDrop.close()
            if (point.x == 0f) {
                pathGradientDrop = Path()
                pathLineDrop = Path()
            }
            drawPath(
                pathGradientDrop,
                brush = gradientVerticalColor,
                alpha = 0.5f
            )
            // and finally we draw the path
            drawPath(
                path,
                color = blueColor30,
                style = Stroke(width = 3f)
            )
            drawPath(
                pathLineDrop,
                color = blueColor,
                style = Stroke(width = 3f)
            )
            drawCircle(
                color = whiteColor,
                center = Offset(x = point.x, y = point.y),
                radius = 7f,
            )
            drawCircle(
                brush = secondaryColor,
                center = Offset(x = point.x, y = point.y),
                radius = 7f,
                style = Stroke(width = 4f)
            )

            drawRoundRect(
                brush = secondaryColor,
                size = Size(width = 73.dp.toPx(), height = 25.dp.toPx()),
                cornerRadius = CornerRadius(x = 36.dp.toPx(), 36.dp.toPx()),
                topLeft = Offset(point.x - 100f, point.y - 100f)
            )
            var pathPointLeft = Path()
            pathPointLeft.moveTo(point.x - 12f, point.y - 50f)
            pathPointLeft.quadraticBezierTo(
                point.x, point.y + 5f, point.x + 12f, point.y - 50f
            )
//            pathPointLeft.lineTo(point.x + 100, point.y - 50);
//            pathPointLeft.quadraticBezierTo(
//                point.x + 150, point.y - 75, point.x + 100, point.y - 100)
//            pathPointLeft.lineTo(point.x + 25, point.y - 45);
//            pathPointLeft.quadraticBezierTo(
//                point.x + 25, point.y - 50, point.x + 20, point.y - 50)
//            pathPointLeft.lineTo(point.x - 80, point.y - 50)
//            pathPointLeft.quadraticBezierTo(
//                point.x - 85, point.y - 50, point.x - 85, point.y - 45)
//            pathPointLeft.lineTo(point.x - 85, point.y - 25);
//            pathPointLeft.quadraticBezierTo(
//                point.x - 85, point.y - 20, point.x - 80, point.y - 20)
//             pathPointLeft.close();
            drawPath(pathPointLeft, brush = secondaryColor)
            drawContext.canvas.nativeCanvas.apply {
                drawText(
                    "3mins ago",
                    point.x,
                    point.y - 55f,
                    Paint().apply {
                        textSize = (10.sp.toPx())
                        color = whiteColor.toArgb()
                        textAlign = Paint.Align.CENTER
                    }
                )
            }

        }) {
//        val offset=IntOffset(offsetX.roundToInt(), offsetY.roundToInt())
        Column(
            modifier = Modifier.padding(
                top = H(LocalSpacing.current.extraSmall),
                start = W(LocalSpacing.current.extraSmall)
            )
        ) {
            Text(
                text = "Heart Rate",
                style = MaterialTheme.typography.body2.copy(fontWeight = FontWeight.W500)
            )
            Text(
                modifier = Modifier
                    .graphicsLayer(alpha = 0.99f)
                    .drawWithCache {
                    onDrawWithContent {
                        drawContent()
                        drawRect(brandColor, blendMode = BlendMode.SrcAtop)
                    }
                },
                text = "${heartRate.bpm} BPM",
                style = MaterialTheme.typography.body1.copy(fontWeight = FontWeight.W600)
            )
        }
//        Box(
//            Modifier
//                .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
//                .background(Color.Blue)
//                .size(50.dp)
//                .pointerInput(Unit) {
//                    detectDragGestures { change, dragAmount ->
//                        change.consumeAllChanges()
//                        offsetX += dragAmount.x
//                        offsetY += dragAmount.y
//                    }
//                }
//        )
    }
}

// simple extension function that allows conversion between ranges
fun Float.mapValueToDifferentRange(
    inMin: Float,
    inMax: Float,
    outMin: Float,
    outMax: Float
) = (this - inMin) * (outMax - outMin) / (inMax - inMin) + outMin

fun getLegacyDateDifference(
    fromDate: String,
    toDate: String,
    formatter: String = "yyyy-MM-dd HH:mm:ss",
    locale: Locale = Locale.getDefault()
): Map<String, Long> {

    val fmt = SimpleDateFormat(formatter, locale)
    val bgn = fmt.parse(fromDate)
    val end = fmt.parse(toDate)

    val milliseconds = end.time - bgn.time
    val days = milliseconds / 1000 / 3600 / 24
    val hours = milliseconds / 1000 / 3600
    val minutes = milliseconds / 1000 / 3600
    val seconds = milliseconds / 1000
    val weeks = days.div(7)

    return mapOf(
        "days" to days,
        "hours" to hours,
        "minutes" to minutes,
        "seconds" to seconds,
        "weeks" to weeks
    )
}

//"yyyy-MM-dd HH:mm:ss"
val heartRates = listOf(
    HeartRate("2022-11-11 13:30:0", 100),
    HeartRate("2022-11-11 13:31:0", 110),
    HeartRate("2022-11-11 13:32:0", 100),
    HeartRate("2022-11-11 13:33:0", 105),
    HeartRate("2022-11-11 13:34:0", 105),
    HeartRate("2022-11-11 13:35:0", 120),
    HeartRate("2022-11-11 13:36:0", 105),
    HeartRate("2022-11-11 13:37:0", 115),
    HeartRate("2022-11-11 13:38:0", 115),
    HeartRate("2022-11-11 13:39:0", 130),
    HeartRate("2022-11-11 13:40:0", 115),
    HeartRate("2022-11-11 13:41:0", 115),
    HeartRate("2022-11-11 13:42:0", 120),
    HeartRate("2022-11-11 13:43:0", 100),
    HeartRate("2022-11-11 13:44:0", 115),
    HeartRate("2022-11-11 13:45:0", 115),
    HeartRate("2022-11-11 13:46:0", 140),
    HeartRate("2022-11-11 13:47:0", 115),
    HeartRate("2022-11-11 13:48:0", 115),
    HeartRate("2022-11-11 13:49:0", 120),
    HeartRate("2022-11-11 13:50:0", 120),
    HeartRate("2022-11-11 13:51:0", 130),
    HeartRate("2022-11-11 13:52:0", 125),
    HeartRate("2022-11-11 13:53:0", 155),
    HeartRate("2022-11-11 13:54:0", 135),
    HeartRate("2022-11-11 13:55:0", 135),
    HeartRate("2022-11-11 13:56:0", 130),
    HeartRate("2022-11-11 13:57:0", 135),
    HeartRate("2022-11-11 13:58:0", 135),
    HeartRate("2022-11-11 13:59:0", 170),
    HeartRate("2022-11-11 14:00:0", 135),
    HeartRate("2022-11-11 14:00:20", 135),
    HeartRate("2022-11-11 14:00:40", 140),
    HeartRate("2022-11-11 14:01:00", 140),
    HeartRate("2022-11-11 14:01:20", 145),
    HeartRate("2022-11-11 14:01:40", 135),
    HeartRate("2022-11-11 14:02:10", 135),
    HeartRate("2022-11-11 14:02:40", 140),
    HeartRate("2022-11-11 14:03:10", 135),
    HeartRate("2022-11-11 14:03:40", 155),
)