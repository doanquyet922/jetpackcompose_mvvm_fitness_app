package com.example.myproject.app.ui.on_boarding

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.whiteColor
import com.example.myproject.R
import com.example.myproject.app.theme.brandColor
import com.example.myproject.app.theme.darkGrayColor
import com.example.myproject.app.ui.common.ButtonCommon
import java.util.Date
import java.util.Calendar

@Composable
fun GetStarted(navigationCallback: () -> Unit) {
    var logoAsset: Int
    var gradientBackground: Brush? = null
    var gradientButton: Brush? = null
    val date = Date()
    val cal = Calendar.getInstance()
    cal.time = date
    val hours = cal.get(Calendar.HOUR_OF_DAY)
    if (hours > 17) {
        logoAsset = R.drawable.fitnest_x
        gradientButton = brandColor
    } else {
        logoAsset = R.drawable.fitnest_x2
        gradientBackground = brandColor
    }
    fun actionGetStarted() {
        navigationCallback()
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(
                brush = gradientBackground ?: Brush.horizontalGradient(
                    listOf(
                        whiteColor,
                        whiteColor
                    )
                )
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            painter = painterResource(logoAsset),
            contentDescription = "",
            modifier = Modifier
                .width(162.dp)
                .height(35.dp)
        )
        Text(
            modifier = Modifier.padding(top = LocalSpacing.current.default),
            text = "Everybody Can Train",
            style = MaterialTheme.typography.subtitle1.copy(color = darkGrayColor)
        )

    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(
                start = LocalSpacing.current.medium,
                end = LocalSpacing.current.medium,
                bottom = LocalSpacing.current.extraLarge
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Bottom
    ) {
        ButtonCommon(
            onClick = { actionGetStarted() },
            text = "Get Started",
            gradientButton = gradientButton,
            gradientText = gradientBackground
        )
    }
}




