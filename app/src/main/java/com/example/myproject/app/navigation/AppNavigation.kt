package com.example.myproject.app.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavOptions
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.myproject.app.ui.dashboard.DashboardScreen
import com.example.myproject.app.ui.intro_register.IntroRegisterScreen
import com.example.myproject.app.ui.login.LoginScreen
import com.example.myproject.app.ui.on_boarding.GetStarted
import com.example.myproject.app.ui.on_boarding.OnBoardingScreen
import com.example.myproject.app.ui.sign_up.SignUpScreen
import com.example.myproject.app.ui.update_profile.UpdateProfileScreen
import com.example.myproject.app.ui.welcome.WelcomeScreen

@Composable
fun AppNavigation() {
    val navController = rememberNavController()
    NavHost(navController, startDestination = Screen.Dashboard.route) {
        composable(route = Screen.GetStarted.route) {
            GetStarted { navController.navigate(Screen.OnBoarding.route) }
        }
        composable(
            route = Screen.OnBoarding.route,
        ) {
            OnBoardingScreen { navController.navigate(Screen.SignUp.route) }
        }
        composable(
            route = Screen.SignUp.route,
        ) {
            SignUpScreen(
                navigationToIntroRegisterCallback = { navController.navigate(Screen.IntroRegister.route) },
                navigationToLoginCallback = {
                    navController.navigate(Screen.Login.route) {
                        popUpTo(
                            Screen.SignUp.route
                        ) { inclusive = true }
                    }
                })
        }
        composable(
            route = Screen.UpdateProfile.route,
        ) {
            UpdateProfileScreen { navController.navigate(Screen.IntroRegister.route) }
        }
        composable(
            route = Screen.IntroRegister.route,
        ) {
            IntroRegisterScreen()
        }
        composable(
            route = Screen.Login.route,
        ) {
            LoginScreen(
                navigationToRegisterCallback = {
                    navController.navigate(Screen.SignUp.route) {
                        popUpTo(
                            Screen.Login.route
                        ) { inclusive = true }
                    }
                },
                navigationToWelcomeCallback = {
                    navController.navigate(Screen.Welcome.route)
                }
            )
        }
        composable(
            route = Screen.Welcome.route,
        ) {
            WelcomeScreen {
                navController.navigate(Screen.Dashboard.route) {
                    popUpTo(
                        Screen.Login.route
                    ) { inclusive = true }
                }
            }
        }
        composable(
            route = Screen.Dashboard.route,
        ) {
            DashboardScreen()
        }
    }
}