package com.example.myproject.app.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

val Shapes = Shapes(
    small = RoundedCornerShape(3.dp),
    medium = RoundedCornerShape(14.dp),
    large = RoundedCornerShape(99.dp),

)