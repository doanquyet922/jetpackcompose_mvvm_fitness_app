package com.example.myproject.app.ui.login

import android.widget.Toast
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myproject.app.data.account.AccountRepository
import com.example.myproject.app.data.account.model.Account
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis

class LoginViewModel : ViewModel() {
    private val accountRepository = AccountRepository.getInstance()
    var account = mutableStateOf<Account?>(null)
    var isLoading = mutableStateOf(false)
    var isLoginSuccess = mutableStateOf(false)
    fun login(
        email: String,
        password: String,
        onLoginSuccess: () -> Unit,
        onLoginFailure: (error: String?) -> Unit
    ) {
        isLoading.value = true
        println("isLoading1:" + isLoading.value)
        viewModelScope.launch(Dispatchers.IO) {
            val response = accountRepository.login(email, password)
            response.onSuccess {
                account.value = response.getOrNull()
                isLoginSuccess.value = true
                onLoginSuccess.invoke()
            }
                .onFailure { ex ->
                    println("login ex:${ex}")
                    onLoginFailure.invoke(ex.message)
                }
            isLoading.value = false
            println("isLoading2:" + isLoading.value)
        }
        println("End")
    }
}