package com.example.myproject.app.ui.sign_up

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import com.example.myproject.R
import com.example.myproject.app.base.constants.LocalSpacing
import com.example.myproject.app.theme.*
import com.example.myproject.app.ui.common.ButtonCommon
import com.example.myproject.app.ui.common.TextFieldCommon

@Composable
fun SignUpScreen(
    navigationToIntroRegisterCallback: () -> Unit,
    navigationToLoginCallback: () -> Unit
) {
    var textFirstName by remember { mutableStateOf("") }
    var textLastName by remember { mutableStateOf("") }
    var textEmail by remember { mutableStateOf("") }
    var textPassword by remember { mutableStateOf("") }
    var checked by remember { mutableStateOf(false) }

    fun changeCheckBox(it: Boolean) {
        checked = it
    }

    fun onPressRegister() {
        Log.d("BBB", "onPressRegister")
        navigationToIntroRegisterCallback()
    }

    fun onPressEmail() {
        Log.d("BBB", "onPressEmail")
    }

    fun onPressFacebook() {
        Log.d("BBB", "onPressFacebook")
    }

    fun toLogin() {
        Log.d("BBB", "toLogin")
        navigationToLoginCallback()
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(
                horizontal = LocalSpacing.current.medium,
                vertical = LocalSpacing.current.extraLarge
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        Text(
            text = "Hey there,",
            style = MaterialTheme.typography.subtitle2,
        )
        Text(
            modifier = Modifier.padding(
                bottom = LocalSpacing.current.medium
            ),
            text = "Create an Account",
            style = MaterialTheme.typography.h4,
        )
        TextFieldCommon(
            value = textFirstName,
            onValueChange = { textFirstName = it },
            placeholderString = "First Name",
            leadingIconVector = ImageVector.vectorResource(R.drawable.ic_profile)
        )
        Spacer(modifier = Modifier.height(LocalSpacing.current.default))
        TextFieldCommon(
            value = textLastName,
            onValueChange = { textLastName = it },
            placeholderString = "Last Name",
            leadingIconVector = ImageVector.vectorResource(R.drawable.ic_profile)
        )
        Spacer(modifier = Modifier.height(LocalSpacing.current.default))
        TextFieldCommon(
            value = textEmail,
            onValueChange = { textEmail = it },
            placeholderString = "Email",
            leadingIconVector = ImageVector.vectorResource(R.drawable.ic_message)
        )
        Spacer(modifier = Modifier.height(LocalSpacing.current.default))
        TextFieldCommon(
            value = textPassword,
            onValueChange = { textPassword = it },
            placeholderString = "Password",
            leadingIconVector = ImageVector.vectorResource(R.drawable.ic_lock)
        )
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.Bottom
        ) {
            Checkbox(
                colors = CheckboxDefaults.colors(
                    uncheckedColor = grayColor
                ),
                checked = checked, onCheckedChange = { changeCheckBox(it) })
            Text(
                text = "By continuing you accept our Privacy Policy and Term of Use",
                style = MaterialTheme.typography.caption.copy(color = grayColor)
            )
        }
        Spacer(
            modifier = Modifier
                .weight(1f)
        )
        ButtonCommon(text = "Register", onClick = { onPressRegister() })
        Row(
            modifier = Modifier.padding(vertical = LocalSpacing.current.extraSmall),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(
                modifier = Modifier
                    .height(1.dp)
                    .weight(1f)
                    .background(color = lightGrayColor)
            )
            Text(
                modifier = Modifier.padding(horizontal = LocalSpacing.current.superExtraSmall),
                text = "Or",
                style = MaterialTheme.typography.body2
            )
            Spacer(
                modifier = Modifier
                    .height(1.dp)
                    .weight(1f)
                    .background(color = lightGrayColor)
            )
        }
        Row {
            Image(
                modifier = Modifier
                    .height(50.dp)
                    .width(50.dp)
                    .clickable { onPressEmail() },
                painter = painterResource(id = R.drawable.ic_gmail),
                contentDescription = ""
            )
            Spacer(modifier = Modifier.width(LocalSpacing.current.medium))
            Image(
                modifier = Modifier
                    .height(50.dp)
                    .width(50.dp)
                    .clickable { onPressFacebook() },
                painter = painterResource(id = R.drawable.ic_facebook),
                contentDescription = ""
            )
        }
        Row(
            modifier = Modifier.padding(
                top = LocalSpacing.current.medium,
            ),
        ) {
            Text(
                text = "Already have an account? ",
                style = MaterialTheme.typography.body1
            )
            Text(
                modifier = Modifier.clickable { toLogin() },
                text = "Login",
                style = MaterialTheme.typography.body1.copy(color = purpleColor)
            )
        }
    }
}

